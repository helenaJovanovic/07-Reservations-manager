![](https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white)
![](https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white)

![](https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white)
![](https://img.shields.io/badge/Express.js-404D59?style=for-the-badge)
![](https://img.shields.io/badge/MongoDB-4EA94B?style=for-the-badge&logo=mongodb&logoColor=white)

# Project Reservations-manager

Aplikacija za rezervaciju termina na terenima sportskog centra

[https://www.youtube.com/watch?v=srcqdODrlWk](https://www.youtube.com/watch?v=srcqdODrlWk)

### Before running: 

Make sure you have Node installed: [Node](https://nodejs.org/en/download/)

Make sure you have MongoDB installed: [MongoDB](https://www.mongodb.com/try/download/community)

Make sure you have Mongo Shell installed: [Mongo Shell](https://www.mongodb.com/try/download/shell)

After that go into `server` folder using terminal or command line and run:

`npm install`

Do the same for `client/reservations-manager-client` folder before proceeding to the next step. This will make sure that all dependencies are installed.

### To run:
Open `server` folder in terminal and run:

`npm run dev`

then go into `client/reservations-manager-client` folder and run:

`ng serve --port 80 --open`

### Database import

Database exports are in `server/db_export`. 

Run: 
`mongoimport --db project] --collection x --file x.json`

for each .json file in the folder where x is the name.

### Admin user:

For testing purposes admin is

username: admin

password: admin

email: admin@admin



## Developers

- [Helena Jovanović, 136/2017](https://gitlab.com/helenaJovanovic)
- [Tina Mladenović, 116/2018](https://gitlab.com/tinamladenovic)
- [Mladen Dilparić, 201/2015](https://gitlab.com/giomla93)
