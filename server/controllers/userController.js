const express = require("express");
const User = require('../model/user');
const bcrypt = require("bcryptjs");
var nodemailer = require('nodemailer');

exports.getById = async(req, res) => {

    const user = await User.findOne({ _id: req.params.user_id });

    res.status(200).json(user.Username);
}

exports.getAll = async(req, res) => {
    const users = await User.find({}).select(['-Password']);
    res.status(200).json(users); 
 }

 exports.putUser = async (req, res) => {
    const id = req.params.id;
    const user = req.body;

    if (id != user._id) {
        return res.status(400).send("Id's don't match");
    }

    User
        .findOneAndUpdate({ _id: req.params.id }, req.body)
        .exec(function (err, oldUser) {
            if (err) return res.status(500).json({ err: err.message });

            res.json({ oldUser, message: 'Successfully updated' })
        });
}

exports.putUserPass = async (req, res) => {
    
    const id = req.params.id;
    var user = await User.findOne({_id: id});

    var EncryptedNew = await bcrypt.hash(req.params.newPass, 10);

    bcrypt.compare(req.params.oldPass, user.Password, (err, result) => {
        return result;
    });

    var isSame = await bcrypt.compare(req.params.oldPass, user.Password);

    if(isSame || req.params.newPass === req.params.oldPass){

        user.Password = EncryptedNew;

        User
        .findOneAndUpdate({ _id: req.params.id }, user)
        .exec(function (err, oldUser) {
            if (err) return res.status(500).json({ err: err.message });

            res.json({ oldUser, message: 'Successfully updated' })
        });
    }
    else
        return res.status(500).json({ err: "Something went wrong" });

}

exports.getMail = async (req, res) => {
    
    const Subject = req.params.subject;
    const Body = req.params.body;
    const user_id = req.params.id;
    var user = await User.findOne({_id: user_id});

    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'sportskiterenimatf@gmail.com',
          pass: 'pzv2021matf'
        },
        tls: {
            rejectUnauthorized: false
        }
      });
  
      var mailOptions = {
        from: 'sportskiterenimatf@gmail.com',
        to: user.Email,
        subject: Subject,
        text: Body
      };
  
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            return res.status(500).json({ err: "Something went wrong" });
        } else {
            return res.status(200).json("Success");
        }
      });


}
