const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("../model/user");
const refreshToken = require("../model/refreshToken");
const auth = require("../middleware/auth");

exports.register =  async (req, res) => {
    try {
        const { Username, FirstName, LastName, Email, Password } = req.body;

        if (!(Username && FirstName && LastName && Email && Password)) {
            res.status(400).send("All fields are required");
        }

        const existingUser = await User.findOne({ Email });

        const existingUser2 = await User.findOne({ Username });

        if (existingUser) {
            return res.status(409).send("Email already in use");
        }

        if(existingUser2){
            return res.status(409).send("Username is already in use");
        }

        Encrypted = await bcrypt.hash(Password, 10);

        const user = await User.create({
            Username,
            FirstName,
            LastName,
            Email: Email.toLowerCase(),
            Password: Encrypted
        });

        //generate token

        const Token = jwt.sign(
            { user_id: user._id, Email: user.Email, Role: user.Role },
            process.env.TOKEN_KEY,
            {
                //change to 5 min when you add refresh tokens
                expiresIn: "2h"
            }
        );

        user.Token = Token;

        let rToken = await refreshToken.createToken(user);

        res.status(201).json({
            Username: user.Username,
            Email: user.Email,
            token: user.Token,
            refreshToken: rToken
        });
    } catch (err) {
        console.log(err);
    }
};

exports.login = async (req, res) => {
    try {

        const { Email, Password } = req.body;

        if (!(Email && Password)) {
            res.status(400).send("Error. Form is incomplete.");
        }

        const user = await User.findOne({ Email });

        if (user && (await bcrypt.compare(Password, user.Password))) {
            const Token = jwt.sign(
                { user_id: user._id, Email: user.Email, Role: user.Role },
                process.env.TOKEN_KEY,
                {
                    expiresIn: "2h"
                }
            );

            user.Token = Token;

            let rToken = await refreshToken.createToken(user);

            res.status(200).json({ token: user.Token, refreshToken: rToken });
        }
        else {
            res.status(400).send("Login failed");
        }
    } catch (err) {
        console.log(err);
    }
};

exports.refreshtoken = async (req, res) => {
    const { token: tkn, refreshToken: rToken } = req.body;

    //we want to refresh only expired jwt tokens

    if (rToken == null || tkn == null) {
        return res.status(403).json({ message: "Token and refresh token are required" });
    }

    let isValid;
    try {
        isValid = jwt.verify(tkn, process.env.TOKEN_KEY);
    } catch (err) {
        //it is supposed to be expired in order to use refreshtoken
        if (err.name == 'TokenExpiredError') {
            //just continue then
        }
        else {
            return res.status(401).send("Token not expired or invalid");
        }
    }

    if (isValid) {
        return res.status(401).send("Token not expired");
    }

    try {
        //We check if such refresh token exists
        let rTokenDb = await refreshToken.findOne({ token: rToken });

        if (!rTokenDb) {
            res.status(403).json({ message: "Refresh token is not in database" });
            return;
        }
        console.log(rTokenDb)

        if (refreshToken.verifyExpiration(rTokenDb)) {
            //used refresh token gets removed
            //TODO: This doesn't work because I can use the same refresh token again...
            refreshToken.findByIdAndRemove(rTokenDb._id, { useFindAndModify: false }).exec();

            res.status(403).json({
                message: "Refresh token was expired. Please make a new signin request",
            });
            return;
        }

        //We are making a new regular token to replace the expired one
        //And making another refreshToken because we used the current one
        let newToken = jwt.sign(
            { user_id: rTokenDb.user._id, Email: rTokenDb.user.Email, Role: rTokenDb.user.Role },
            process.env.TOKEN_KEY,
            {
                expiresIn: "2h"
            }
        );

        let rToken2 = await refreshToken.createToken(rTokenDb.user);

        //Remove the used refresh token. It can be used only once
        refreshToken.findByIdAndRemove(rTokenDb._id, { useFindAndModify: false }).exec();


        return res.status(200).json({
            token: newToken,
            refreshToken: rToken2
        })

    } catch (err) {
        return res.status(500).send({ message: err.message });
    }
}