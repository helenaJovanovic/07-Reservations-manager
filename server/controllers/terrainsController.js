const express = require("express");
const Terrain = require('../model/terrain');



exports.terrainPost = async (req, res) => {
    try{
        let { Name, Sport, Price} = req.body;

        if(!Name || !Sport || !Price){
            return res.status(400).send("Fields are missing");
        }

        const terrainExists = await Terrain.findOne({ Name: Name });
        if(terrainExists){
            return res.status(400).send("Cant add already existing terrain");
        }

        const terrain = await Terrain.create({
            Name: Name,
            Sport: Sport,
            Price: Price
        }).catch((error) => {
            return res.status(500).send(error);
        });

        return res.status(201).json(terrain);
    }
    catch(err){
        console.log(err);
    }
}


exports.terrainPut = async (req, res) => {
    const id = req.params.id;
    const terrain = req.body;

    if (id != terrain._id) {
        return res.status(400).send("Id's don't match");
    }

    Terrain
        .findOneAndUpdate({ _id: req.params.id }, req.body)
        .exec(function (err, oldTerrain) {
            if (err) return res.status(500).json({ err: err.message });

            res.json({ oldTerrain, message: 'Successfully updated' })
        });
}

exports.terrainDelete = async (req, res) => {
    Terrain.findByIdAndDelete(req.params.id).then(
        (terrain) => {
            if(!terrain){
                return res.status(404).send();
            }
            res.send(terrain);
        })
        .catch((error) => {
            res.status(500).send(error);
        });
}


exports.getAll = async (req, res) => {
    const records = await Terrain.find({});
    res.status(200).json(records);
}

exports.getByTerrainId = async (req, res) => {
    const records = await Terrain.findOne({_id: req.params.id});
    res.status(200).json(records);
}