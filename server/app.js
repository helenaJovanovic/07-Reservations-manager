require("dotenv").config();
require("./config/database").connect();

const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("./model/user");
const refreshToken = require("./model/refreshToken");
const terrain = require("./model/terrain");
const reservation = require("./model/reservation")
const auth = require("./middleware/auth");
var cors = require('cors')


var authRouter = require('./routes/authRoute')
var reservationRouter = require('./routes/reservationRoute');
var terrainsRouter = require('./routes/terrainsRoute');
var usersRouter = require('./routes/userRoute');

const app = express();

app.use(cors());

app.use(express.json());

app.use('/auth/', authRouter);

app.use('/terrains/', terrainsRouter );
app.use('/reservations/', reservationRouter);

app.use('/users/', usersRouter);

//Za sad da bi se koristila autorizacija samo se doda ovaj auth izmedju rute i funkcije
//Postoji i nacin da se proveri da li je korisnik user ili admin

//radi samo uz autorizaciju
app.get("/welcome", auth, (req, res) => {
    res.status(200).send("Welcome");
});

//radi bez autorizacije
app.get("/welcome2", (req, res) => {
    res.status(200).send("Welcome");
});

//Default ruta ako nista od gore nije trazeno
app.use("*", (req, res) => {
    res.status(404).json({
        success: "false",
        message: "Page not found",
        error: {
            statusCode: 404,
            message: "Doesn't exist"
        }
    });
});

module.exports = app;