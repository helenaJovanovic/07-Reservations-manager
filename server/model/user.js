const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    Username: {type: String, unique: true},
    FirstName: {type: String, default: null},
    LastName: {type: String, default:null},
    Email: {type: String, unique: true},
    Password: {type: String},
    Token: {type: String},
    Role: {type:String, default: "user"}
});

//Za sad postoji nacin samo da se registruje user a ne admin, jer se default namesta vrednost polja na user
//Ako bismo hteli da napravim admin usera, mozemo da promenimo polje user is mongodb compasa za sad

module.exports = mongoose.model("user", userSchema);
