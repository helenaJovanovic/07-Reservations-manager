const mongoose = require("mongoose");

const reservationSchema = new mongoose.Schema({
    //Monoose can only store UTC time
    //Send time in format like this 2012-11-04T15:00+01:00 when doing POST and PUT
    
    //When sending data from database it will be converted from UTC to UTC+1 zone

    reservation_date_time: {type: Date, default:null},
    end_date_time: {type: Date, default:null},
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    username: {
        type: String, default: ""
    },
    _terrain_id: {type: mongoose.Schema.Types.ObjectId, default:null, ref: 'terrain'},
    price: {type: Number, default: 0},
    //in minutes
    //example when from local +1 it gets converted to utc, time_zone will be -60 (minutes)
    time_zone: {type: Number, default: 0},
    duration: {type: Number, default: 0}
});

reservationSchema.index({ reservation_date_time: 1, _terrain_id: 1 }, { unique: true });

module.exports = mongoose.model("reservation", reservationSchema);
