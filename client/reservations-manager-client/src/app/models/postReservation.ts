export class postReservation{
    reservationDateTime: Date | undefined;
    Username: string | undefined;
    terrainId: string | undefined;
    Duration: number | undefined;

    constructor(reservationDateTime: Date, Username: string, terrainId: string, Duration: number){
        this.reservationDateTime = reservationDateTime;
        this.Username = Username;
        this.terrainId = terrainId;
        this.Duration = Duration;
    }
}