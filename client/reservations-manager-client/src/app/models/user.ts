export class User{
    _id: string | undefined;
    Username: string | undefined;
    FirstName:string | undefined;
    LastName: string | undefined;
    Email: string | undefined;
    Password: string | undefined;
    Token: string | undefined;
    Role: string | undefined;
}
