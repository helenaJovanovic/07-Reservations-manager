export class Reservation{
    _id: string | undefined;
    reservation_date_time: Date | undefined;
    end_date_time: Date | undefined;
    user_id: string | undefined;
    username: string | undefined;
    _terrain_id: string | undefined;
    price: number | undefined;
    time_zone: number | undefined;
    duration: number | undefined;
}