import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Observable, Subscription } from 'rxjs';
import { Reservation } from 'src/app/models/reservation';
import { Terrain } from 'src/app/models/terrain';
import { ReservationsService } from 'src/app/shared/reservations.service';
import { UsersService } from 'src/app/shared/users.service';
import { TerrainsService } from 'src/app/shared/terrains.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  Tereni: Terrain[];
  Users: User[];
  user: User;
  reservations_user: Reservation[];
  Reservations: Reservation[];
  subscriptions: Subscription[];

  terreniForma = this.fb.group({
    Name: [''],
    Sport: [''],
    Price: 0
  })

  terreniForma2 = this.fb.group({
    Id: [''],
    Name: [''],
    Sport: [''],
    Price: 0
  })

  pretragaForma = this.fb.group({
    niskaPretrage: ['']
  })

  userForma = this.fb.group({
    Username: [''],
    Firstname: [''],
    Lastname: [''],
    Email: ['']
  })

  passForma = this.fb.group({
    NewPass: ['']
  })

  mejlForma = this.fb.group({
    Subject: [''],
    Body: ['']
  })

  terrainIdToName: any = {};

  constructor(private terrainsService: TerrainsService, private fb: FormBuilder,
    public ngxSmartModalService: NgxSmartModalService, private http: HttpClient,
    private reservationsService: ReservationsService, private usersService: UsersService) {
    this.Tereni = [];
    this.subscriptions = [];
    this.reservations_user = [];
  }

  ngOnInit(): void {
    this.refreshTerrains();
  }

  pretrazi(){
    this.Reservations = [];
    var username = this.pretragaForma.get("niskaPretrage").value;

    if(username === ''){
      return ;
    }

    this.subscriptions.push(
      this.reservationsService.getByUsername(username).subscribe(
        (res) => {
          this.Reservations = res;
          this.getTerrainNames();
        }
      )
    )


  }

  setReservations(u: User){
    this.subscriptions.push(
      this.reservationsService.getByUserId(u._id, true).subscribe(
        (res) => {
          this.reservations_user = res;
        }
      )
    )
  }

  refreshTerrains() {
    this.subscriptions.push(

      this.terrainsService.getAllTerrains().subscribe(
        (res) => {
          this.Tereni = res;
        }
      )

    )
  }

  dateToLocalString(date: Date) {
    var tmp : string = moment(date).locale('sr').format("dddd, MMMM Do YYYY, h:mm:ss a");
    return tmp.substring(0, tmp.length-6) + tmp.substring(tmp.length-3);
  }

  obrisiRez(id: string){
    if (confirm("Da li ste sigurni da želite da obrišete ovu rezervaciju?")) {
      this.subscriptions.push(
        this.reservationsService.deleteReservation(id).subscribe(
          (res) => {
            this.Reservations = this.Reservations.filter(x => x._id != id)
          }
        )
      )
    }
  }

  izmeniT(t: Terrain){
    this.terreniForma2.setValue({
      Id: t._id,
      Name: t.Name,
      Price: t.Price,
      Sport: t.Sport
    })

    this.terreniForma2.get("Id").disable();

    this.ngxSmartModalService.getModal('myModal').open();

  }

  obrisiT(t: Terrain) {
    if (confirm("Da li ste sigurni da želite da obrišete ovaj teren?")) {
      this.subscriptions.push(
        this.terrainsService.deleteTerrain(t._id).subscribe(
          (res) => {
            this.Tereni = this.Tereni.filter(x => x._id != t._id)
          }
        ),
        this.reservationsService.getByTerrainId(t._id).subscribe(
          (res) => {
            res.forEach(reservation => { this.reservationsService.deleteReservation(reservation._id).subscribe(
              (res) => {
                this.Reservations = this.Reservations.filter(x => x._id != reservation._id)
              }
            )
          }
          )
          }
      )
      )
  }
}

  izmeniTeren(){

    var Id = this.terreniForma2.get("Id").value;
    var Ime = this.terreniForma2.get("Name").value;
    var Sport = this.terreniForma2.get("Sport").value;
    var Price = this.terreniForma2.get("Price").value;

    var terrain = new Terrain(Id, Ime, Sport, Price);

    this.subscriptions.push(
      this.terrainsService.putTerrain(terrain).subscribe(
        (res) => {
          this.refreshTerrains();
        },
        (error) => {
          window.alert("Došlo je do greške: " + error.err);
        }
      )
    )
    this.ngxSmartModalService.getModal('myModal').close();
  }

  noviTeren() {
    var Ime = this.terreniForma.get("Name").value;
    var Sport = this.terreniForma.get("Sport").value;
    var Price = this.terreniForma.get("Price").value;

    if(Ime === '' || Sport === '' || Price === 0){
      window.alert("Forma ne sme da ima prazno polje");
      return ;
    }

    this.subscriptions.push(
      this.terrainsService.postNewTerrain(Ime, Sport, Price).subscribe(
        (res) => {
          window.alert("Novi teren je uspešno dodat");
          this.terreniForma.reset();
        },
        (error) => {
          window.alert("Došlo je do greške: " + error.err);
        }
      )
    )
  }


  getUsersObject(){
    this.subscriptions.push(
      this.usersService.getAllUsers().subscribe(
        (res) => {
          this.Users = res;
        }
      )
    )
  }

  getTerrainNames(){

    this.Reservations.forEach(x => {
      if(!(x._terrain_id in this.terrainIdToName)){
        this.subscriptions.push(
          this.terrainsService.getByTerrainId(x._terrain_id).subscribe(
            (res) => {
              this.terrainIdToName[x._terrain_id] = res.Name;
            }
          )
        )
      }
    })

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  izmeniK(u: User){
    this.userForma.setValue({
      Username: u.Username,
      Firstname: u.FirstName,
      Lastname: u.LastName,
      Email: u.Email
    })

    this.user = u;
    this.setReservations(u);

    this.ngxSmartModalService.getModal('myModal1').open();
  }

  izmeniKorisnika(){
    var Username = this.userForma.get("Username").value;
    var Firstname = this.userForma.get("Firstname").value;
    var Lastname = this.userForma.get("Lastname").value;
    var Email = this.userForma.get("Email").value;

    this.user.Username = Username;
    this.user.FirstName = Firstname;
    this.user.LastName = Lastname;
    this.user.Email = Email;


    this.subscriptions.push(
      this.usersService.putUser(this.user).subscribe(
        (res) => {
          this.refreshUsers();
        },
        (error) => {
          window.alert("Došlo je do greške: " + error.err);
        }
      )
    )

    this.reservations_user.forEach(element => {

      element.username = Username;

      this.subscriptions.push(
        this.reservationsService.putReservation(element).subscribe(
          (res) => {
          },
          (error) => {
            window.alert("Došlo je do greške: " + error.err);
          }
        )
      )
  });


    this.ngxSmartModalService.getModal('myModal1').close();
  }


  refreshUsers() {
    this.getUsersObject();
  }

  izmeniPass(u: User){

    this.passForma.setValue({
      NewPass: ['']
    });

    this.user = u;
    this.ngxSmartModalService.getModal('myModal3').open();

  }

  izmeniPassword(){


    var NewPass = this.passForma.get("NewPass").value;

    this.subscriptions.push(
      this.usersService.putUserPass(this.user,  NewPass, NewPass).subscribe(
        (res) => {
          this.refreshUsers();
          this.ngxSmartModalService.getModal('myModal3').close();
        },
        (error) => {
          window.alert("Došlo je do greške");
        }
      )
    )


  }

  prikaziMejl(u: User){

    this.mejlForma.setValue({
      Subject: [''],
      Body: ['']
    });

    this.user = u;
    this.ngxSmartModalService.getModal('myModal4').open();
  }

  posaljiMejl(){
    var Subject = this.mejlForma.get("Subject").value;
    var Body = this.mejlForma.get("Body").value;


    this.subscriptions.push(
      this.usersService.getMail(this.user._id,  Subject, Body).subscribe(
        (res) => {
          this.ngxSmartModalService.getModal('myModal4').close();
        },
        (error) => {
          window.alert("Došlo je do greške");
        }
      )
    )

  }

}
