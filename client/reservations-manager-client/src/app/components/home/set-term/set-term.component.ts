import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Terrain } from 'src/app/models/terrain';
import { AuthService } from 'src/app/shared/auth.service';
import { TerrainsService } from 'src/app/shared/terrains.service';

@Component({
  selector: 'app-set-term',
  templateUrl: './set-term.component.html',
  styleUrls: ['./set-term.component.css']
})


export class SetTermComponent implements OnInit {

  terminiForm: FormGroup;

  subscriptions: Subscription[];

  terrains: Terrain[];

  currentTerrainId: string;
  currentPrice: number;

  constructor(
    public fb: FormBuilder,
    public authService: AuthService,
    public router: Router,
    public http: HttpClient,
    private terrainsService: TerrainsService
  ) {



    this.subscriptions = [];

    //TODO: Replace this with terrain service when its ready
    this.subscriptions.push(this.terrainsService.getAllTerrains().subscribe(
      (res) => {
        this.terrains = res;
      },
      (error) => {
        window.alert("Error fetching terrains from database");
      }
    ))
  }

  ngOnInit(): void {
    this.terminiForm = this.fb.group({
      terrainId: [''],
      dateTime: ['']
    });

    this.onChanges();
  }

  onChanges(): void {
    this.subscriptions.push(
      this.terminiForm.get("terrainId").valueChanges.subscribe(id => {
        console.log(id);
        this.currentTerrainId = id;
        if (id != "Izaberite teren") {
          this.currentPrice = this.terrains.find(x => x._id == id).Price;
        }
      })
    )
  }

  registerAppointment(): void {
    console.log(this.terminiForm.get('dateTime')?.value);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe());
  }

}
