import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelTermComponent } from './cancel-term.component';

describe('CancelTermComponent', () => {
  let component: CancelTermComponent;
  let fixture: ComponentFixture<CancelTermComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CancelTermComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelTermComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
