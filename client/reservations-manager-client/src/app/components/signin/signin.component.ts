import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscriber, Subscription } from 'rxjs';
import { AuthService } from 'src/app/shared/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  signinForm: FormGroup;
  signInSubscription: Subscription | null;

  constructor(public fb: FormBuilder,
    public authService: AuthService,
    public router: Router) {

    this.signInSubscription = null;

    this.signinForm = this.fb.group({
      Email: [''],
      Password: ['']
    })
  }

  ngOnInit(): void {
  }

  signInUser() {
    this.signInSubscription = this.authService.signIn(this.signinForm.value);
  }

  navigateToSignUp() {
    this.router.navigate(['signup']);
  }

  ngOnDestroy(){
    if(this.signInSubscription !== null)
    {
      this.signInSubscription.unsubscribe();
    }
  }

}
