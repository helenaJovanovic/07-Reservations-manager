import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { postReservation } from '../models/postReservation';
import { Reservation } from '../models/reservation';

@Injectable({
  providedIn: 'root'
})
export class ReservationsService {

  endpoint: string = environment.url;
  headers = new HttpHeaders().set('Content-Type', 'application-json');

  constructor(private http: HttpClient) { }


//vraca vreme u UTC po default ali ako se pozove pomocna funkcija odavde nad Reservation tipom
//dobije se string sa lokalnim vremenom

//primer: Dobije se lista sa objektima Reservation tipa od GET /getAll
//nad pojedinacnim Reservation objektom se pozove this.reservationService.toLocalTime(reservation);
//vraca string u lokalnom vremenu

/*
Primer poziva getAllReservations nakon injektovanja

res: any;
this.reservationService.getAllReservations().subscribe((res) => {
        this.AllReservations = res;
      })

  res je onda lista Objekata koji su ustvari Reservation objekti

*/
  getAllReservations() {
    let path = this.endpoint + '/' + 'reservations' + '/' + 'getAll';

    return this.http.get<Reservation>(path);
  }

  /*
    Ovo je primer upotrebe nakon injecktovanja ovog servisa

    this.reservationService.postNewReservation(new Date("2012-11-04T13:00"), "test", "Veliki fudbalski teren").subscribe(
      (res) => {
        console.log(res)
        //obrada greske ide ovde
      }
    );

  */
  postNewReservation(datum: Date, username: string, terrainId: string, Duration: number) {

    var reservation = new postReservation(datum, username, terrainId, Duration);

    let path = this.endpoint + '/' + 'reservations' + '/' + 'addReservation2';

    return this.http.post(path, reservation);
  }

  //Get individual reservation by reservation id
  getByReservationId(id: string) : Observable<Reservation>{
    let path = this.endpoint + '/' + 'reservations' + '/' + 'getByReservationId/' + id;

    return this.http.get<Reservation>(path);
  }

  //vraca listu objekata
  getByUsername(username: string) {
    let path = this.endpoint + '/' + 'reservations' + '/' + 'getByUsername/' + username;

    return this.http.get<Reservation[]>(path);
  }

  //Vraca listu objekata
  getByUserId(userId: string, history: boolean)
  {
    var all = 0;
    if(history)
      all = 1;
    let path = this.endpoint + '/' + 'reservations' + '/' + 'getByUserId/' + userId + '/' + all;

    return this.http.get<Reservation[]>(path);
  }

  //vraca listu objekata
  getByTerrainId(terrainId: string) {
    let path = this.endpoint + '/' + 'reservations' + '/' + 'getByTerrainId/' + terrainId;

    return this.http.get<Reservation[]>(path);
  }

  deleteReservation(reservationId: string){
    let path = this.endpoint + '/' + 'reservations' + '/' + 'removeReservation/' + reservationId;

    return this.http.delete<Reservation>(path);

  }

  //for updating
  //argument is Reservation object you changed
  //response is success or fail
  //must have reservation _id in the Reservation object
  putReservation(reservation: Reservation){
    let path = this.endpoint + '/' + 'reservations' + '/' + 'changeReservation/' + reservation._id;

    return this.http.put(path, reservation);
  }

  //helper function
  toLocalTime(reservation: Reservation) {
    var date = moment(reservation.reservation_date_time);
    return date.local().toString();
  }
}
