import { NgModule } from '@angular/core';
import { RouterModule, Routes,RouterOutlet } from '@angular/router';
import { CancelTermComponent } from './components/home/cancel-term/cancel-term.component';
import { ChangeInfoComponent } from './components/home/change-info/change-info.component';
import { HomeComponent } from './components/home/home.component';
import { SetTermComponent } from './components/home/set-term/set-term.component';
import { SigninComponent } from './components/signin/signin.component';
import { SignupComponent } from './components/signup/signup.component';
import { SettingsComponent } from './components/home/settings/settings.component';
import { AuthGuard } from './shared/auth.guard';
import { LogedinGuard } from './shared/logedin.guard';
import { AdminGuard } from './shared/admin.guard';

//logedin guard - prevents loged in user from accessing the page
//Auth huard - prevents user that didn't log in from accessing the page
const routes: Routes = [
  { path: '', redirectTo: '/signin', pathMatch: 'full' },
  { path: 'signin', component: SigninComponent, canActivate: [LogedinGuard]},
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard],
  children: [
    {path: 'setTermin', component:SetTermComponent},
    {path: 'cancelTermin',component: CancelTermComponent},
    {path: 'changeInfo',component: ChangeInfoComponent},
    {path: 'settings', component: SettingsComponent, canActivate: [AdminGuard]}
  ]},
  { path: 'signup', component: SignupComponent, canActivate: [LogedinGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }
